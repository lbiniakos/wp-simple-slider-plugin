This is a simple Slider plugin created during online courses.
The user creates slides (custom posts), changes slide style and uses a shortcode to display the slider.

PS: Don't forget to change the folder name from "wp-simple-slider-plugin" to "mv-slider" in order to work properly
